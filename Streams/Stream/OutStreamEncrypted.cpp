#include "OutStreamEncrypted.h"
#include <string.h>
#pragma warning (disable:4996)

#define MIN_VAL 32
#define MAX_VAL 126


OutStreamEncrypted::OutStreamEncrypted(int offset) :
	OutStream(), _offset(offset)
{
}


OutStreamEncrypted::~OutStreamEncrypted()
{
}

char* OutStreamEncrypted::Encrypt(char* str)
{
	char* result = str;
	
	for (int i = 0; i < strlen(str); i++)
	{
		if (result[i] >= MIN_VAL && result[i] <= MAX_VAL)
		{
			result[i] += this->_offset;
		}
	}

	return result;
}

int OutStreamEncrypted::CountDigits(int num)
{
	int res = 0;
	while (num > 0)
	{
		res++;
		num /= 10;
	}

	return res;
}


OutStreamEncrypted& OutStreamEncrypted::operator<<(const char* str)
{
	char* toPrint = new char[strlen(str)]; 
	strcpy(toPrint, str);

	fprintf(this->_file, "%s", Encrypt(toPrint));

	return *this;
}


OutStreamEncrypted& OutStreamEncrypted::operator<<(const int num)
{
	char* toPrint = new char[CountDigits(num)];
	sprintf(toPrint, "%d", num);

	fprintf(this->_file, "%s", Encrypt(toPrint));

	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(void(*pf)(FILE*))
{
	pf(this->_file);
	return *this;
}