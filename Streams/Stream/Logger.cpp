#include "Logger.h"
#include "OutStream.h"
#include <stdio.h>



unsigned int Logger::_logCount = 0;

Logger::Logger()
{
	this->os = OutStream();
	this->_startLine = true;
}

Logger::~Logger()
{
}


Logger& operator<<(Logger& l, const char* msg)
{
	if (l._startLine)
	{
		l.setStartLine(false);
		l.os << Logger::_logCount << ". LOG: ";
		Logger::_logCount++;
	}
	l.os << msg;

	return l;
}


Logger& operator<<(Logger& l, int num)
{
	if (l._startLine)
	{
		l.setStartLine(false);
		l.os << Logger::_logCount << ". LOG: ";
		Logger::_logCount++;
	}
	l.os << num;

	return l;
}


Logger& operator<<(Logger& l, void(*pf)(FILE*))
{
	pf(l.os._file);
	l.setStartLine(true);
	return l;
}

void Logger::setStartLine(bool s)
{
	this->_startLine = s;
}