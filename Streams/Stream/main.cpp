#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"

#define KEY 3

int main(int argc, char **argv)
{
	OutStream os = OutStream();
	FileStream fs = FileStream("ex.txt");
	OutStreamEncrypted ose = OutStreamEncrypted(KEY);
	Logger l = Logger();

	os << "I am the doctor and im " << 1500 << " years old" << endline;
	fs << "I am the doctor and im " << 1500 << " years old" << endline;
	ose << "My name is Tair" << endline;
	l << "This is log 1 " << "See how im controling the lines" << endline;
	l << "And this is Log 2! Amazing";

	return 0;
}

