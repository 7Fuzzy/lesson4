#pragma once
#include "OutStream.h"

namespace msl
{
	class FileStream :
		public OutStream
	{
	public:
		FileStream(const char* path);
		~FileStream();
	};

}