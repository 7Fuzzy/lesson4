#pragma once
#include <stdio.h>

namespace msl
{
	class OutStream
	{
	public:
		FILE *_file;

		OutStream();
		~OutStream();

		OutStream& operator<<(const char *str);
		OutStream& operator<<(int num);
		OutStream& operator<<(void(*pf)(FILE*));
	};

	void endline(FILE* stream);
}